# Anomia

Made with Dax89/electron-sveltekit

## Installation

```
Clone repository, and run `yarn install` (or `npm install`)
```

## Commands
- `npm run dev`: Runs SvelteKit in dev mode
- `npm run start`: Runs SvelteKit in production mode
- `npm run electron`: Runs SvelteKit with electron in dev mode
- `npm run build`: Runs SvelteKit compiler
- `npm run dev:package`: Creates an Electron package (you can inspect the contents)
- `npm run package`: Creates a distributable Electron package

